"""Pig latin scrript"""

def get_first_letter(word):
    """get first letter of word"""
    return word[0]


def is_vowel(word):
    """
    define letter is vowel and return it bool
    Aarguments:
        string: letter - tested letter
    Returns:
        bool: is_vowel
    """
    vowels = ["a", "A", "e", "E", "i", "I", "o", "O", "u", "U"]
    return get_first_letter(word) in vowels


def translate_vowel(word):
    """translate words with vowel first letter"""
    return "{}{}".format(word, "way")


def translate_consonant(word):
    """translate words with consonant first letter"""
    first_letter = get_first_letter(word)
    word_part = word[1:]
    suffix = "ay"
    return "{}{}{}".format(word_part, first_letter, suffix)


def main(word):
    """
    return word translated to pig latin
    Attributes:
        string: word
    Returns:
        string: translated
    """
    if is_vowel(word):
        translated = translate_vowel(word)
    else:
        translated = translate_consonant(word)
    print(translated)



if __name__ == "__main__":
    while True:
        print("\nEnter some work to get pig latin translation\n")
        print("\nOr n to quite\n")
        input_word = input("Write a word and press enter:\n")
        if input_word.lower() != 'n':
            main(input_word)
        else:
            break
