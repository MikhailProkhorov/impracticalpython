"""return letters of sentence usage"""


def result_output(result):
    """print formated result"""
    for i in result:
        print("{}: {}".format(i, result[i]))


def main(sentence):
    """return letters of sentence usage"""
    result = {}
    for character in sentence:
        letter = character.strip().lower()
        if len(letter) == 0:
            continue

        result[letter] = []

    for character in sentence:
        letter = character.strip().lower()
        if len(letter) == 0:
            continue

        result[letter].append(letter)

    result_output(result)


if __name__ == "__main__":
    while True:
        inputSentence = input("\nInput sentence or 'n' to quite:")
        if inputSentence != 'n':
            main(inputSentence)
        else:
            break
